#ifndef __INIP_HASHMAP_H
#define __INIP_HASHMAP_H

#include <stdlib.h>
#include <stdint.h>

//default size of 128, non-resizable
#define INIP_HM_SIZE 128

//Using linked-list because I'm lazy
typedef struct t_HashMap HashMap;

HashMap* hm_create(void);
void hm_destruct(HashMap* table);
void hm_set(HashMap* table, const char* key, void* value);
void* hm_get(const HashMap* table, const char* key, void* default_value);
int hm_size(const HashMap* table);

#endif
