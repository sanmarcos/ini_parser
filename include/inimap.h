#ifndef __INIP_INIMAP_H
#define __INIP_INIMAP_H

#include "hashmap.h"

typedef struct t_KeyValuePairs KeyValuePairs;

typedef struct{
    HashMap*       group_map;
    KeyValuePairs* groups;
    KeyValuePairs* attributes;
}IniMap;

IniMap* im_create(void);
void im_destruct(IniMap* map);
//creates a new group and returns the hashmap of the newly added group
HashMap* im_add_group(IniMap* map, const char* group_name);
HashMap* im_get_group(IniMap* map, const char* group_name);
void  im_set_attribute(IniMap* map, HashMap* group, const char* attribute, const char* value);
char* im_get_attribute(IniMap* map, const char* path);

#endif
