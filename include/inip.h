#ifndef __INIP_H
#define __INIP_H

#include <stdbool.h>
#include <string.h>

typedef struct t_INIFile INIFile;

INIFile* inip_create(void);
void     inip_destruct(INIFile* ini);
bool     inip_load(INIFile* ini, const char* filename);
bool     inip_load_from_memory(INIFile* ini, const char* data, size_t size);
bool     inip_has_group(const INIFile* ini, const char* key);
bool     inip_has_attribute(const INIFile* ini, const char* key);
bool     inip_is_list(const INIFile* ini, const char* key);
bool     inip_is_string(const INIFile* ini, const char* key);
bool     inip_is_num(const INIFile* ini, const char* key);

//Value getters
int                inip_get_int(INIFile* ini, const char* key, int default_value);
unsigned long      inip_get_ul(INIFile* ini, const char* key, unsigned long default_value);
unsigned long long inip_get_ull(INIFile* ini, const char* key, unsigned long long default_value);
float              inip_get_float(INIFile* ini, const char* key, float default_value);
double             inip_get_double(INIFile* ini, const char* key, double default_value);
char*              inip_get_string(INIFile* init, const char* key, const char* default_value);

bool inip_get_ints(INIFile* ini, const char* key, size_t size_ints, int* ints);
bool inip_get_uls(INIFile* ini, const char* key, size_t size_ints, unsigned long* ints);
bool inip_get_ulls(INIFile* ini, const char* key, size_t size_ints, unsigned long long* ints);
bool inip_get_floats(INIFile* ini, const char* key, size_t size_floats, float* floats);
bool inip_get_doubles(INIFile* ini, const char* key, size_t size_doubles, double* doubles);
bool inip_get_strings(INIFile* ini, const char* key, size_t size_strings, char** strings);

#endif
