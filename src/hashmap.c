#include "include/hashmap.h"
#include <string.h>
#include <stdio.h>
#include <stdbool.h>

static const uint64_t offset_basis = 14695981039346656037u;
static const uint64_t FNV_prime    = 1099511628211u;
static const uint64_t mask         = INIP_HM_SIZE - 1;

//static inline char* str_tolower(const char* str){
//    char* new_str = malloc(strlen(str) + 1);
//    strcpy(new_str, str);
//    for(char* c = new_str; *c != '\0'; ++c){
//        *c = tolower(*c);
//    }
//    return new_str;
//}

typedef struct t_Node Node;
struct t_Node{
    const char* key;
    void*       data;
    Node*       next;
    uint64_t    hash;
};

struct t_HashMap{
    Node** nodes;
    int n_nodes;
};

uint64_t hashf(const char* key){
    uint64_t hash = offset_basis;
    for(const char* c = key; *c != '\0'; ++c){
        hash *= FNV_prime;
        hash ^= *c;
    }
    return hash;
}

HashMap* hm_create(void){
    HashMap* table = malloc(sizeof(HashMap));
    table->nodes   = malloc(INIP_HM_SIZE * sizeof(Node*));
    table->n_nodes = 0;
    for(int i = 0; i < INIP_HM_SIZE; ++i){
        table->nodes[i] = NULL;
    }
    return table;
}

void hm_destruct(HashMap* table){
    for(int i = 0; i < INIP_HM_SIZE; ++i){
        Node* node = table->nodes[i];
        while(node){
            Node* next_node = node->next;
            free(node);
            node = next_node;
        }
    }
    free(table->nodes);
    free(table);
}

int hm_size(const HashMap* table){
    return table->n_nodes;
}

void hm_set(HashMap* table, const char* key, void* value){
    uint64_t hash = hashf(key);
    uint64_t idx  = hash & mask;

    Node* node = table->nodes[idx];
    if(!node){
        node = malloc(sizeof(Node));
        table->nodes[idx] = node;
        ++table->n_nodes;
    }
    else{
        bool exists = false;
        if(hash == node->hash && strcmp(key, node->key) == 0) exists = true;
        while(node->next){
            node = node->next;
            if(hash == node->hash && strcmp(key, node->key) == 0) exists = true;
        }

        if(!exists){
            node->next = malloc(sizeof(Node));
            node = node->next;
            ++table->n_nodes;
        }
    }

    node->data = value;
    node->key  = key;
    node->hash = hash;
    node->next = NULL;
}

void* hm_get(const HashMap* table, const char* key, void* default_value){
    uint64_t hash = hashf(key);
    uint64_t idx  = hash & mask;

    Node* node = table->nodes[idx];

    if(!node) return default_value;
    for(Node* node = table->nodes[idx]; node != NULL; node = node->next){
        if(node->hash == hash && strcmp(node->key, key) == 0){
            return node->data;
        }
    }

    return default_value;
}
