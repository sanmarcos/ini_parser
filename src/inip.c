#include "include/inip.h"
#include "include/inimap.h"
#include <stdio.h>
#include <ctype.h>

struct t_INIFile{
    IniMap* map;
};

INIFile* inip_create(void){
    INIFile* ini_file = malloc(sizeof(*ini_file));
    ini_file->map = im_create();
    return ini_file;
}

void inip_destruct(INIFile* ini){
    im_destruct(ini->map);
    free(ini);
}

static inline void trim_ws(char* string){
    const char* start = string;
    const char* end = string + strlen(string) - 1;
    while(isspace(*start)) ++start;
    while(isspace(*end) && end > start) --end;
    size_t size = end - start + 1;
    memmove(string, start, size);
    string[size] = '\0';
}

static inline bool is_string(const char* string){
    const char* start = string;
    const char* end = string + strlen(string) - 1;

    if(!(*start == '\'' || *start == '\"')) return false;

    if((*start == '\'' && !(*end == '\'')) ||
       (*start == '\"' && !(*end == '\"')))
    {
        fprintf(stderr, "Unmatched quotes in %s\n", string);
        return false;
    }

    return true;
}

static inline void trim_quotes(char* string){
    const char* start = string;
    const char* end = string + strlen(string) - 1;
    bool is_trimmed = false;
    if(*start == '\'' || *start == '\"'){
        ++start;
        is_trimmed = true;
    }
    if((*end == '\'' || *end == '\"') && end > start){
        --end;
        is_trimmed = true;
    }
    if(is_trimmed){
        size_t size = end - start + 1;
        memmove(string, start, size);
        string[size] = '\0';
    }
}

static inline const char* stream_getline(char* line, int size, const char* stream, int stream_size){
    int ch = 0;
    char* buff = line;
    while((--size > 0) && (--stream_size > 0)){
        if ((*buff++ = stream[ch++]) == '\n') break; 
    }
    //TODO: Had a problem when no new line at end of file
    //This should be a temporary fix. At least try to
    //understand why it works, and if it is the best
    //way to solve the problem. Note that i had to add 1
    //to stream_size at the caller(in parse()).
    if(stream_size == 0) *buff++ = stream[ch++];
    *buff = '\0';
    return (buff == line) ? NULL: stream + ch;
}

static inline void parse(INIFile* ini, const char* data, int data_size){
    char line[256];
    HashMap* group = NULL;
    const char* data_ptr = data;
    while((data_ptr = stream_getline(line, sizeof(line), data_ptr, data_size + data - data_ptr + 1)) != NULL){
        switch(line[0]){
            default:{
                if(group){
                    char* equal_sign = strchr(line, '=');
                    if(equal_sign){
                        size_t key_size = equal_sign - line - 1;
                        char key[key_size + 1];
                        char value[strlen(equal_sign + 1) + 1];
                        memcpy(key, line, key_size);
                        key[key_size] = '\0';
                        trim_ws(key);
                        strcpy(value, equal_sign + 1);
                        trim_ws(value);
                        im_set_attribute(ini->map, group, key, value);
                    }
                }
                break;
            }
            case '[':{
                char* pos = strchr(line, ']');
                if(pos){
                    size_t size = pos - line;
                    char group_name[size];
                    memcpy(group_name, line + 1, size - 1);
                    group_name[size - 1] = '\0';
                    group = im_add_group(ini->map, group_name);
                }
                break;
            }
            case '#': break;
        }
    }
}

bool inip_load(INIFile* ini, const char* filename){
    FILE* file = fopen(filename, "rb");
    if(!file) return false;

    fseek(file, 0L, SEEK_END);
    size_t sz = ftell(file);
    char* buffer = malloc(sz);

    fseek(file, 0L, SEEK_SET);
    fread(buffer, sz, 1, file);
    fclose(file);

    parse(ini, buffer, sz / sizeof(char) - 1);

    free(buffer);
    return true;
}

bool inip_load_from_memory(INIFile* ini, const char* data, size_t sz){
    if(!data) return false;
    parse(ini, data, sz);
    return true;
}

bool inip_has_group(const INIFile* ini, const char* key){
    HashMap* group = im_get_group(ini->map, key);
    if(!group) return false;

    return (hm_size(group) > 0);
}

bool inip_has_attribute(const INIFile* ini, const char* key){
    return (im_get_attribute(ini->map, key) != NULL);
}

bool inip_is_list(const INIFile* ini, const char* key){
    char* string = im_get_attribute(ini->map, key);
    if(!string) return false;

    char* start = strchr(string, '[');
    if(!start) return false;

    char* end = strchr(start + 1, ']');
    if(!end) return false;

    return true;
}

bool inip_is_string(const INIFile* ini, const char* key){
    char* string = im_get_attribute(ini->map, key);
    if(!string) return false;

    return is_string(string);
}

bool inip_is_num(const INIFile* ini, const char* key){
    char* string = im_get_attribute(ini->map, key);
    if(!string) return false;

    return strlen(string) == strspn(string, "0123456789.e-+");
}

#define INIP_GET_BASIC(short_name, type, func)\
    type inip_get_ ## short_name(INIFile* ini, const char* key, type default_value){\
        char* attr = im_get_attribute(ini->map, key);\
        return attr? func: default_value;\
    }

INIP_GET_BASIC(int, int, atoi(attr))
INIP_GET_BASIC(float, float, atof(attr))
INIP_GET_BASIC(double, double, strtod(attr, NULL))
INIP_GET_BASIC(ul, unsigned long, strtoul(attr, NULL, 0))
INIP_GET_BASIC(ull, unsigned long long, strtoull(attr, NULL, 0))

char* inip_get_string(INIFile* ini, const char* key, const char* default_value){
    char* ret_string;
    char* attr = im_get_attribute(ini->map, key);
    if(attr && is_string(attr)){
        ret_string = malloc(strlen(attr) + 1);
        strcpy(ret_string, attr);
        trim_quotes(ret_string);
    }
    else{
        ret_string = malloc(strlen(default_value) + 1);
        strcpy(ret_string, default_value);
    }

    return ret_string;
}

bool inip_get_strings(INIFile* ini, const char* key, size_t size_strings, char** strings){
    char* attr = im_get_attribute(ini->map, key);
    if(!attr) return false;

    char* start = strchr(attr, '[');
    if(!start) return false;

    char* end = strchr(start + 1, ']');
    if(!end) return false;

    size_t size = end - start;
    char string[size];
    memcpy(string, start + 1, size - 1);
    string[size - 1] = '\0';

    char* comma = string;
    char* pos = string;
    size_t count = 0;
    while((comma = strchr(pos, ',')) && count < size_strings){
        *comma = '\0';
        trim_ws(pos);
        if(is_string(pos)) trim_quotes(pos);
        strings[count] = malloc(strlen(pos) + 1);
        strcpy(strings[count], pos);
        pos = comma + 1;
        ++count;
    }
    if(count < size_strings){
        trim_ws(pos);
        if(is_string(pos)) trim_quotes(pos);
        strings[count] = malloc(strlen(pos) + 1);
        strcpy(strings[count], pos);
    }
    return true;
}

#define INIP_GET_BASIC_LIST(short_name, type, func)\
    bool inip_get_ ## short_name ## s(INIFile* ini, const char* key, size_t size_vals, type* vals){\
        char* strings[size_vals];\
        if(!inip_get_strings(ini, key, size_vals, strings)) return false;\
        for(size_t i = 0; i < size_vals; ++i){\
            vals[i] = func;\
            free(strings[i]);\
        }\
        return true;\
    }\

INIP_GET_BASIC_LIST(int, int, atoi(strings[i]))
INIP_GET_BASIC_LIST(double, double, strtod(strings[i], NULL))
INIP_GET_BASIC_LIST(float, float, atof(strings[i]))
INIP_GET_BASIC_LIST(ul, unsigned long, strtoul(strings[i], NULL, 0))
INIP_GET_BASIC_LIST(ull, unsigned long long, strtoull(strings[i], NULL, 0))

