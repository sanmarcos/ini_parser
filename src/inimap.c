#include "include/inimap.h"
#include <string.h>

struct t_KeyValuePairs{
    void** data;
    char** keys;
    size_t size;
    size_t reserve;
};

static inline KeyValuePairs* kvp_create(void){
    KeyValuePairs* kvp = malloc(sizeof(*kvp));
    kvp->size    = 0;
    kvp->reserve = 2;
    kvp->data    = malloc(kvp->reserve * sizeof(void*));
    kvp->keys    = malloc(kvp->reserve * sizeof(char*));

    return kvp;
}

static inline void kvp_destruct(KeyValuePairs* kvp){
    free(kvp->data);
    free(kvp->keys);
    free(kvp);
}

static inline void kvp_add(KeyValuePairs* kvp, char* key, void* value){
    if(kvp->size + 1 >= kvp->reserve){
        kvp->reserve *= 2;
        kvp->keys = realloc(kvp->keys, kvp->reserve * sizeof(char*));
        kvp->data = realloc(kvp->data, kvp->reserve * sizeof(void*));
    }
    kvp->keys[kvp->size] = key;
    kvp->data[kvp->size] = value;
    ++kvp->size;
}

IniMap* im_create(void){
    IniMap* ini_map = malloc(sizeof(IniMap));
    ini_map->group_map = hm_create();

    ini_map->groups     = kvp_create();
    ini_map->attributes = kvp_create();

    return ini_map;
}

void im_destruct(IniMap* map){
    hm_destruct(map->group_map);

    for(int i = 0; i < map->groups->size; ++i){
        hm_destruct((HashMap*)map->groups->data[i]);
        free(map->groups->keys[i]);
    }
    kvp_destruct(map->groups);

    for(int i = 0; i < map->attributes->size; ++i){
        free(map->attributes->keys[i]);
        free(map->attributes->data[i]);
    }
    kvp_destruct(map->attributes);

    free(map);
}

HashMap* im_add_group(IniMap* map, const char* group_name){
    char* saved_name = malloc(strlen(group_name) + 1);
    strcpy(saved_name, group_name);
    HashMap* new_group = hm_create();

    kvp_add(map->groups, saved_name, new_group);

    hm_set(map->group_map, saved_name, new_group);

    return new_group;
}

HashMap* im_get_group(IniMap* map, const char* group_name){
    return hm_get(map->group_map, group_name, NULL);
}

void im_set_attribute(IniMap* map, HashMap* group, const char* attr_name, const char* value){
    char* saved_name  = malloc(strlen(attr_name) + 1);
    char* saved_value = malloc(strlen(value) + 1);
    strcpy(saved_name, attr_name);
    strcpy(saved_value, value);

    kvp_add(map->attributes, saved_name, saved_value);

    hm_set(group, saved_name, saved_value);
}

char* im_get_attribute(IniMap* map, const char* path){
    int i;
    for(i = 0; path[i] != '.'; ++i){
        if(path[i] == '\0') return NULL;
    }
    char* group_name = malloc(i + 1);
    memcpy(group_name, path, i);
    group_name[i] = '\0';

    char* attr_name = malloc(strlen(path + i + 1) + 1);
    strcpy(attr_name, path + i + 1);

    HashMap* group = im_get_group(map, group_name);
    char* attr = group? hm_get(group, attr_name, NULL): NULL;

    free(attr_name);
    free(group_name);

    return attr;
}

