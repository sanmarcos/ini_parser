#include <stdio.h>
#include <stdlib.h>
#include "include/inip.h"

#define KRED  "\x1B[31m"
#define KNRM  "\x1B[0m"
#define KGRN  "\x1B[32m"

static int num_tested = 0;
static int num_passed = 0;
#define DEF_TEST(name) static bool test_##name
#define TEST(function, ...) \
    ++num_tested;\
    printf("Testing %-20.20s... ", #function);\
    if(test_##function(__VA_ARGS__)){\
        ++num_passed;\
        printf("%sPassed%s\n", KGRN, KNRM);\
    }\
    else printf("%sFailed%s\n", KRED, KNRM);

#define TEST_CRITICAL(fail_func, function, ...)\
    ++num_tested;\
    printf("Testing %-20.20s... ", #function);\
    if(test_##function(__VA_ARGS__)){\
        ++num_passed;\
        printf("%sPassed%s\n", KGRN, KNRM);\
    }\
    else{\
        printf("\nCritical failure. Aborting tests.\n");\
        fail_func(__VA_ARGS__);\
        exit(0);\
    }

static void clean_up(INIFile* ini, const char* data, size_t size){
    inip_destruct(ini);
}

static void clean_up_file(INIFile* ini, const char* filename){
    inip_destruct(ini);
    remove(filename);
}

DEF_TEST(load)(INIFile* ini, const char* data, size_t size){
    return inip_load_from_memory(ini, data, size);
}

DEF_TEST(load_file)(INIFile* ini, const char* filename){
    return inip_load(ini, filename);
}

DEF_TEST(has_group)(INIFile* ini){
    return inip_has_group(ini, "group") && inip_has_group(ini, "new_group");
}

DEF_TEST(has_group_failure)(INIFile* ini){
    return !(inip_has_group(ini, "dummy_group") && inip_has_group(ini, "dummy_group2"));
}

DEF_TEST(has_attribute)(INIFile* ini){
    bool all_attributes = inip_has_attribute(ini, "group.double") &&
                          inip_has_attribute(ini, "group.int") &&
                          inip_has_attribute(ini, "group.string") &&
                          inip_has_attribute(ini, "new_group.double") &&
                          inip_has_attribute(ini, "new_group.strings") &&
                          inip_has_attribute(ini, "new_group.ints");

    return all_attributes;
}

DEF_TEST(has_attribute_failure)(INIFile* ini){
    bool all_attributes = inip_has_attribute(ini, "group.dummy") &&
                          inip_has_attribute(ini, "new_group.dummy")&&
                          inip_has_attribute(ini, "dummy.dummy");

    return !all_attributes;
}

DEF_TEST(is_list)(INIFile* ini){
    return inip_is_list(ini, "new_group.ints") && inip_is_list(ini, "new_group.strings");
}

DEF_TEST(is_list_failure)(INIFile* ini){
    bool all = inip_is_list(ini, "group.int") &&
               inip_is_list(ini, "group.string") &&
               inip_is_list(ini, "new_group.double") &&
               inip_is_list(ini, "group.double");
    return !all;
}

DEF_TEST(is_string)(INIFile* ini){
    return inip_is_string(ini, "group.string");
}

DEF_TEST(is_string_failure)(INIFile* ini){
    bool all = inip_is_string(ini, "group.int") &&
               inip_is_string(ini, "group.double") &&
               inip_is_string(ini, "new_group.strings") &&
               inip_is_string(ini, "new_group.double");
    return !all;
}

DEF_TEST(is_num)(INIFile* ini){
    return inip_is_num(ini, "group.double") &&
           inip_is_num(ini, "group.scientific") &&
           inip_is_num(ini, "group.int");
}

DEF_TEST(is_num_failure)(INIFile* ini){
    bool all = inip_is_num(ini, "group.string") &&
               inip_is_num(ini, "new_group.strings") &&
               inip_is_num(ini, "new_group.ints");
    return !all;
}

DEF_TEST(is_group_empty)(INIFile* ini){
    return !inip_has_group(ini, "empty_group");
}

DEF_TEST(get_int)(INIFile* ini){
    int int_value = inip_get_int(ini, "group.int", -1);
    return (int_value == 3);
}

DEF_TEST(get_int_failure)(INIFile* ini){
    int int_value = inip_get_int(ini, "group.dummy", -1);
    return (int_value == -1);
}

DEF_TEST(get_double)(INIFile* ini){
    double double_value = inip_get_double(ini, "group.double", -1.0);
    return (double_value == 15.8);
}

DEF_TEST(get_double_failure)(INIFile* ini){
    double double_value = inip_get_double(ini, "group.dummy", -1.0);
    return (double_value == -1.0);
}

DEF_TEST(get_double2)(INIFile* ini){
    double double_value = inip_get_double(ini, "new_group.double", -1.0);
    return (double_value == 12.1);
}

DEF_TEST(get_double_failure2)(INIFile* ini){
    double double_value = inip_get_double(ini, "new_group.dummy", -1.0);
    return (double_value == -1.0);
}

DEF_TEST(get_string)(INIFile* ini){
    char* string = inip_get_string(ini, "group.string", "None");
    bool passed = strcmp(string, "Hello World!") == 0;
    free(string);
    return passed;
}

DEF_TEST(get_string_failure)(INIFile* ini){
    char* string = inip_get_string(ini, "group.dummy", "None");
    bool passed = strcmp(string, "None") == 0;
    free(string);
    return passed;
}

DEF_TEST(get_badstring1_failure)(INIFile* ini){
    char* string = inip_get_string(ini, "group.bad_string1", "None");
    bool passed = strcmp(string, "None") == 0;
    free(string);
    return passed;
}

DEF_TEST(get_badstring2_failure)(INIFile* ini){
    char* string = inip_get_string(ini, "group.bad_string2", "None");
    bool passed = strcmp(string, "None") == 0;
    free(string);
    return passed;
}

DEF_TEST(get_badstring3_failure)(INIFile* ini){
    char* string = inip_get_string(ini, "group.bad_string3", "None");
    bool passed = strcmp(string, "None") == 0;
    free(string);
    return passed;
}

DEF_TEST(get_ints)(INIFile* ini){
    int my_list[] = {14, 12, 13, 5};
    int expected[] = {1, 2, 3, 4};
    inip_get_ints(ini, "new_group.ints", 4, my_list);
    bool passed = true;
    for(int i = 0; i < 4; ++i){
        if(my_list[i] != expected[i]){
            passed = false;
            break;
        }
    }
    return passed;
}

DEF_TEST(get_ints_failure)(INIFile* ini){
    int my_list[] = {14, 12, 13, 5};
    int expected[] = {14, 12, 13, 5};
    inip_get_ints(ini, "new_group.dummy", 4, my_list);
    bool passed = true;
    for(int i = 0; i < 4; ++i){
        if(my_list[i] != expected[i]){
            passed = false;
            break;
        }
    }
    return passed;
}

DEF_TEST(get_strings)(INIFile* ini){
    char* strings[2] = {"not", "found"};
    char* expected[2] = {"Hello", "World"};
    bool found = inip_get_strings(ini, "new_group.strings", 2, strings);
    bool passed = true;
    for(int i = 0; i < 2; ++i){
        if(strcmp(strings[i], expected[i]) != 0){
            passed = false;
            break;
        }
    }
    if(found){
        free(strings[0]);
        free(strings[1]);
    }
    return passed;
}

DEF_TEST(get_strings_failure)(INIFile* ini){
    char* strings[2] = {"not", "found"};
    char* expected[2] = {"not", "found"};
    bool found = inip_get_strings(ini, "new_group.dummy", 2, strings);
    bool passed = true;
    for(int i = 0; i < 2; ++i){
        if(strcmp(strings[i], expected[i]) != 0){
            passed = false;
            break;
        }
    }
    if(found){
        free(strings[0]);
        free(strings[1]);
    }
    return passed;
}

int main(int argc, char* argv[]){

    freopen("/dev/null", "w", stderr);

    const char test_ini[] = 
        "#Comment\n"
        "[group]\n"
        "int   = 3\n"
        "double = 15.8\n"
        "scientific = 15.8e-3\n"
        "string = \"Hello World!\"\n"
        "bad_string1 = \"Hello World!\n"
        "bad_string2 = Hello World!\"\n"
        "bad_string3 = Hello World!\n"
        "\n"
        "[new_group]\n"
        "double = 12.1\n"
        "ints  = [1, 2, 3, 4]\n"
        "strings = [\"Hello\", \"World\"]";

    FILE* file = fopen("test.ini", "w");
    fprintf(file, "%s", test_ini);
    fclose(file);

    {
        INIFile* ini = inip_create();

        TEST_CRITICAL(clean_up, load, ini, test_ini, sizeof(test_ini));
        TEST(has_group, ini);
        TEST(has_group_failure, ini);
        TEST(has_attribute, ini);
        TEST(has_attribute_failure, ini);
        TEST(is_list, ini);
        TEST(is_list_failure, ini);
        TEST(is_string, ini);
        TEST(is_string_failure, ini);
        TEST(is_num, ini);
        TEST(is_num_failure, ini);
        TEST(is_group_empty, ini);
        TEST(get_int, ini);
        TEST(get_int_failure, ini);
        TEST(get_double, ini);
        TEST(get_double_failure, ini);
        TEST(get_double2, ini);
        TEST(get_double_failure2, ini);
        TEST(get_string, ini);
        TEST(get_string_failure, ini);
        TEST(get_badstring1_failure, ini);
        TEST(get_badstring2_failure, ini);
        TEST(get_badstring3_failure, ini);
        TEST(get_ints, ini);
        TEST(get_ints_failure, ini);
        TEST(get_strings, ini);
        TEST(get_strings_failure, ini);

        clean_up(ini, test_ini, sizeof(test_ini));
    }
    {
        INIFile* ini = inip_create();

        TEST_CRITICAL(clean_up_file, load_file, ini, "test.ini");
        TEST(has_group, ini);
        TEST(has_group_failure, ini);
        TEST(has_attribute, ini);
        TEST(has_attribute_failure, ini);
        TEST(is_list, ini);
        TEST(is_list_failure, ini);
        TEST(is_string, ini);
        TEST(is_string_failure, ini);
        TEST(is_num, ini);
        TEST(is_num_failure, ini);
        TEST(is_group_empty, ini);
        TEST(get_int, ini);
        TEST(get_int_failure, ini);
        TEST(get_double, ini);
        TEST(get_double_failure, ini);
        TEST(get_double2, ini);
        TEST(get_double_failure2, ini);
        TEST(get_string, ini);
        TEST(get_string_failure, ini);
        TEST(get_badstring1_failure, ini);
        TEST(get_badstring2_failure, ini);
        TEST(get_badstring3_failure, ini);
        TEST(get_ints, ini);
        TEST(get_ints_failure, ini);
        TEST(get_strings, ini);
        TEST(get_strings_failure, ini);

        clean_up_file(ini, "test.ini");
    }

    printf("Finished testing. Passed %d/%d tests.\n", num_passed, num_tested);

    return 0;
}
