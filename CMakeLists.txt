cmake_minimum_required(VERSION 2.8)
project(ini_parser)

include_directories(.)
set(CMAKE_C_FLAGS "-Wall -O3 -std=c99 -g -march=native")

set(SOURCE_FILES 
    src/hashmap.c src/inimap.c src/inip.c)

add_library(ini_parser STATIC ${SOURCE_FILES})

add_executable(test EXCLUDE_FROM_ALL test/test.c)
target_link_libraries(test ini_parser)

install(TARGETS ini_parser DESTINATION ${PROJECT_SOURCE_DIR}/bin/)
install(FILES include/inip.h DESTINATION ${PROJECT_SOURCE_DIR}/bin/)

